part of 'news_bloc.dart';

@immutable
abstract class NewsEvent extends Equatable{
  const NewsEvent();
  @override
  List<Object?> get props => [];
}

class GetListNewsEvent extends NewsEvent{
final int count;
final bool sendRequest;

  const GetListNewsEvent(this.count, this.sendRequest);

@override
List<Object?> get props => [count, sendRequest];
}
