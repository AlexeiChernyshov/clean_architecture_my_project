import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

import '../../../../core/services/service_locator.dart';
import '../../model/data/models/news_model.dart';
import '../../model/services/news_screen_service.dart';


part 'news_event.dart';
part 'news_state.dart';

final _newsScreenService = instanceStorage<NewsScreenService>();

class NewsBloc extends Bloc<NewsEvent, NewsState> {

  NewsBloc() : super(NewsInitialState()) {

    on<GetListNewsEvent>(_onGetListNews);
    // Запрос на получение списка новостей при инициализации блока
    add(const GetListNewsEvent(3, false));
  }

  _onGetListNews(GetListNewsEvent event, Emitter<NewsState> emit) async {
    // Проверка: если на экране уже есть загруженное состояние, MainScreenLoadingState не показываем
    if (state is! NewsLoadedState){
      emit(NewsLoadingState());
    }
    // Если в сервисе уже хранится список новостей
    if (_newsScreenService.newsList.isNotEmpty){
      try {
        await _newsScreenService.getListNewsMethod(event.count, event.sendRequest);
      } catch (e) {
        print('Error $e');
      } finally {
        /*
         В любом случае когда в сервисе хранится список новостей,
         даже если при запросе произошол сбой, берем список новостей
         хранящийся в сервисе
         */
        emit(NewsLoadedState(_newsScreenService.newsList));
      }

    } else {
      try {
        await _newsScreenService.getListNewsMethod(event.count, event.sendRequest);
        emit(NewsLoadedState(_newsScreenService.newsList));
      } catch (e) {
        emit(NewsLoadingFailureState(exception: e));
        print('Error $e');
      }
    }

  }

}
