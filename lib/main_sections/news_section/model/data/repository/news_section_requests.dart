import '../../../../../core/data/resources/method_constants.dart';
import '../../../../../core/data/resources/request_post_function.dart';
import '../models/news_model.dart';

class NewsSectionRequests{

  Future<List<NewsModel>> getNews({
    required int count,
  }) async {
    final Map<String, dynamic> params = {
      "offset": 0,
      "count": count,
      "city_id": ''};

    List<NewsModel> parseJson(dynamic json) {
      final jsonMap = json as Map<String, dynamic>;
      final listObject = <NewsModel>[];
      if (jsonMap.containsKey('result')) {
        final resultJson = List<NewsModel>.from((jsonMap["result"] as List)
            .map((e) => NewsModel.fromJson(e)));
        return resultJson;
      } else {
        return listObject;
      }
    }

    final result = functionRequest(
        params: params, method: MethodConstants.getNews, parser: parseJson);
    return result;
  }
}