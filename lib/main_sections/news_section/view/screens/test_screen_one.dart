import 'package:flutter/material.dart';

import '../../../../core/presentation/components/image_with_shimmer.dart';
import '../../../../core/resources/app_strings.dart';
import '../../model/data/repository/news_image_url.dart';


class TestScreenOneWidget extends StatelessWidget {
  const TestScreenOneWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: Center(
        child: Column(
          children: [
            const Text(NewsSectionStrings.testScreenOne),
            const SizedBox(height: 20),
            SizedBox(
              height: 400,
              width: 400,
              child: ClipRRect(
                borderRadius: BorderRadius.circular(10),
                child: const ImageWithShimmer(
                  imageUrl: NewsImageUrl.imageScreenOne,
                  width: 400,
                  height: double.infinity,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
