import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:go_router/go_router.dart';

import '../../../../core/resources/app_routes.dart';
import '../../../../core/resources/app_strings.dart';
import '../../controllers/news_bloc/news_bloc.dart';
import '../components/view_news_list_widget.dart';


// Обертка экрана
class WrapperNewsScreenWidget extends StatelessWidget {
  const WrapperNewsScreenWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider<NewsBloc>(
      create: (context) => NewsBloc(),
        child: const NewsScreenWidget());
  }
}

// Виджет дерево главного экрана
class NewsScreenWidget extends StatelessWidget {
  const NewsScreenWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(
          children: [
            const SizedBox(height: 30,),
            const ViewNewsListWidget(),
            const SizedBox(height: 30,),
            TextButton(
              onPressed: () => context.goNamed(AppRoutes.testScreenOneRoute),
                child: const Text(
                    NewsSectionStrings.goToTestScreenOne),),
          ],
        ),
      ),
    );
  }
}

