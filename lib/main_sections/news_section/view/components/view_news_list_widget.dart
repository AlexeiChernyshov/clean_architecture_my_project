import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../../core/presentation/components/view_button_widget.dart';
import '../../../../core/resources/app_strings.dart';
import '../../controllers/news_bloc/news_bloc.dart';
import '../../model/data/models/news_model.dart';

class ViewNewsListWidget extends StatelessWidget {
  const ViewNewsListWidget({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final bloc = BlocProvider.of<NewsBloc>(context);

    void incrementNews(count) {
      count += 5;
      bloc.add(GetListNewsEvent(count, true));
    }

    return BlocBuilder<NewsBloc, NewsState>(
        builder: (context, state) {
          if (state is NewsInitialState){
            return const Center(child: Text(NewsSectionStrings.downloadNews,),);
          }
          else if (state is NewsLoadedState) {
            final newsList = state.listNews;
            return SizedBox(
              height: 330,
              child: ListView.builder(
                scrollDirection: Axis.horizontal,
                clipBehavior: Clip.none,
                physics: const BouncingScrollPhysics(),
                itemCount: newsList.length + 1,
                itemBuilder: (BuildContext context, int index) {
                  if(index == newsList.length) {
                    return InkWell(
                      onTap: () => incrementNews(newsList.length),
                      child: const ViewButtonWidget(
                        height: 300,
                        width: 150,
                        buttonIcon: Icon(
                            Icons.add_circle,
                            color: Colors.orange),),);
                  }
                  return _CreateNewsListCardsWidget(
                    index: index,
                    listNews: newsList,
                  );
                },
              ),
            );
          }
          else if (state is NewsLoadingState) {
            return const Center(child: CircularProgressIndicator());
          }
          else if (state is NewsLoadingFailureState) {
            return Center(child: Column(
              children: [
                Text(state.exception.toString()),
                TextButton(
                  onPressed: () => bloc.add(const GetListNewsEvent(3, true)),
                  child: const Text(NewsSectionStrings.update,),)
              ],
            ));
          }
          else {
            return const Text('');
          }
        });
  }
}

// Конструктор списка карточек новостей
class _CreateNewsListCardsWidget extends StatelessWidget {
  final int index;
  final List<NewsModel> listNews;

  const _CreateNewsListCardsWidget({Key? key, required this.index, required this.listNews})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    final object = listNews[index];
    return _NewsWidget(
        imageNews: object.miniPhoto ?? '',
        dateNews: object.startDate ?? '',
        nameNews: object.title ?? '');
  }
}

// Виджет карточки новости
class _NewsWidget extends StatelessWidget {
  final String imageNews; // фото новости
  final String dateNews; // дата новости
  final String nameNews; // текст новости

  const _NewsWidget({
    required this.imageNews,
    required this.dateNews,
    required this.nameNews,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: const BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(17.0)),
        boxShadow: [
          BoxShadow(
            color: Colors.white38,
            offset: Offset(0, 8),
            blurRadius: 12,
            spreadRadius: 0,
          ),
        ],
      ),
      margin: const EdgeInsets.only(right: 15.0),
      width: 160,
      child: Column(
        children: [
          SizedBox(
            height: 140,
            width: 160,
            child: ClipRRect(
              borderRadius: const BorderRadius.only(
                topRight: Radius.circular(17.0),
                topLeft: Radius.circular(17.0),
              ),
              child: Image.network(
                imageNews,
                fit: BoxFit.cover,
              ),
            ),
          ),
          SizedBox(
            child: Padding(
              padding: const EdgeInsets.all(
                15,
              ),
              child: Column(
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Text(
                        dateNews,
                      ),
                    ],
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Flexible(
                        child: Text(
                          nameNews,
                          softWrap: true,
                          maxLines: 5,
                          overflow: TextOverflow.ellipsis,
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}

