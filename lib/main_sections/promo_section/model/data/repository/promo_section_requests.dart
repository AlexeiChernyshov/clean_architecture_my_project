import '../../../../../core/data/resources/method_constants.dart';
import '../../../../../core/data/resources/request_post_function.dart';
import '../models/promo_model.dart';

class PromoSectionRequest{

  Future<List<PromoModel>> getPromo({
    required int count,
  }) async {
    final Map<String, dynamic> params = {
      "offset": 0,
      "count": count,
      "city_id": ''};
    List<PromoModel> parseJson(dynamic json) {
      final jsonMap = json as Map<String, dynamic>;
      final listObject = <PromoModel>[];
      if (jsonMap.containsKey('result')) {
        final resultJson = List<PromoModel>.from((jsonMap["result"] as List)
            .map((e) => PromoModel.fromJson(e)));
        return resultJson;
      } else {
        return listObject;
      }
    }

    final result = functionRequest(
        params: params, method: MethodConstants.getPromo, parser: parseJson);
    return result;
  }
}