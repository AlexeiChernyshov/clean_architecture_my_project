import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

import '../../../../core/services/service_locator.dart';
import '../../../../core/utils/enums.dart';
import '../../model/data/models/promo_model.dart';
import '../../model/services/promo_screen_service.dart';

part 'promo_event.dart';
part 'promo_state.dart';

final _promoScreenService = instanceStorage<PromoScreenService>();

class PromoBloc extends Bloc<PromoEvent, PromoState> {
  PromoBloc() : super(const PromoState()) {
    on<GetListPromoEvent>(_getPromo);
  }

  Future<void> _getPromo(
      GetListPromoEvent event, Emitter<PromoState> emit) async {
    if(_promoScreenService.promoList.isNotEmpty){
      try {
        emit(state.copyWith(
          status: RequestStatus.loaded,
          listPromo: _promoScreenService.promoList,
        updateProgress: true),);
        await _promoScreenService.getListPromoMethod(event.count, event.sendRequest);
      } catch (e) {
        print('Error $e');
      } finally {
        emit(state.copyWith(
          status: RequestStatus.loaded,
          listPromo: _promoScreenService.promoList,),);
      }
    } else {
      emit(state.copyWith(
        status: RequestStatus.loading),);
      try {
        await _promoScreenService.getListPromoMethod(event.count, event.sendRequest);
        emit(state.copyWith(
          status: RequestStatus.loaded,
          listPromo: _promoScreenService.promoList,),);
      }  catch (e) {
        print('Error $e');
        emit(
          state.copyWith(
            status: RequestStatus.error,
          ),
        );
      }
    }
  }
}
