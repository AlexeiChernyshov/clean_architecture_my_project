import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:go_router/go_router.dart';

import '../../../../core/resources/app_routes.dart';
import '../../../../core/resources/app_strings.dart';
import '../../controllers/promo_bloc/promo_bloc.dart';
import '../components/view_promo_list_widget.dart';


class WrapperPromoScreenWidget extends StatelessWidget {
  const WrapperPromoScreenWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider<PromoBloc>(
        create: (context) => PromoBloc()..add(const GetListPromoEvent(3, false)),
        child: const PromoScreenWidget());
  }
}

class PromoScreenWidget extends StatelessWidget {
  const PromoScreenWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Center(
          child: Column(
            children: [
              const SizedBox(height: 30,),
              const ViewPromoListWidget(),
              const SizedBox(height: 30,),
              TextButton(
                onPressed: () => context.goNamed(AppRoutes.testScreenTwoRoute),
                child: const Text(
                    PromoSectionStrings.goToTestScreenTwo),)
            ],
          ),
        ),
      ),
    );
  }
}


