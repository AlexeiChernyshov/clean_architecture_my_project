import 'package:flutter/material.dart';

import '../../../../core/presentation/components/image_with_shimmer.dart';
import '../../../../core/resources/app_strings.dart';
import '../../model/data/repository/promo_image_url.dart';


class TestScreenThreeWidget extends StatelessWidget {
  const TestScreenThreeWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: Center(
        child: Column(
          children: [
            const Text(PromoSectionStrings.testScreenThree),
            const SizedBox(height: 20,)
,            SizedBox(
          height: 400,
          width: 400,
          child: ClipRRect(
            borderRadius: BorderRadius.circular(10),
            child: const ImageWithShimmer(
              imageUrl: ImageUrl.imageScreenThree,
              width: 400,
              height: double.infinity,
            ),
          ),
        ),
          ],
        ),
      ),
    );
  }
}

