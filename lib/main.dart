 import 'dart:io';

import 'package:flutter/material.dart';

import 'core/services/service_locator.dart';
import 'core/my_app.dart';

 class MyHttpOverrides extends HttpOverrides {
   @override
   HttpClient createHttpClient(SecurityContext? context) {
     return super.createHttpClient(context)
       ..badCertificateCallback =
           (X509Certificate cert, String host, int port) => true;
   }
 }

void main() {
  // Инициализация GetIt
  ServiceLocator.initLocator();
  HttpOverrides.global = MyHttpOverrides();
  WidgetsFlutterBinding.ensureInitialized();
  runApp(const MyApp());
}




