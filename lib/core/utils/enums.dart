// NetworkError - ошибка сети
// WrongData - неверные данные
// OtherError - другие ошибки
// ServerError - ошибка сервера (500)
// TimeoutError - ошибка превышения времени ошидания ответа сервера

enum HttpClientExceptionType {
  networkError,
  wrongData,
  otherError,
  serverError,
  timeoutError
}

enum RequestStatus {
  loading,
  loaded,
  error }