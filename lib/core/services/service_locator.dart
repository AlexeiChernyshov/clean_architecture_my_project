import 'package:get_it/get_it.dart';

import '../../main_sections/news_section/model/services/news_screen_service.dart';
import '../../main_sections/promo_section/model/services/promo_screen_service.dart';

GetIt instanceStorage = GetIt.instance;

class ServiceLocator{
  static void initLocator() {
    instanceStorage.registerSingleton<NewsScreenService>(NewsScreenService(),
        signalsReady: true);
    instanceStorage.registerSingleton<PromoScreenService>(PromoScreenService(),
        signalsReady: true);
  }
}