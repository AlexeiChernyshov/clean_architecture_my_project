// кнопка смотреть всё
import 'package:flutter/material.dart';

class ViewButtonWidget extends StatelessWidget {
  final double height;
  final double width;
  final Widget buttonIcon;

  const ViewButtonWidget({
    Key? key,
    required this.height,
    required this.width,
    required this.buttonIcon,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        border: Border.all(
          color: Colors.orange,
          width: 0.5,
        ),
        borderRadius: const BorderRadius.all(Radius.circular(17.0)),
      ),
      height: height,
      width: width,
      child: Padding(
        padding: const EdgeInsets.all(
          15,
        ),
        child: buttonIcon,
      ),
    );
  }
}