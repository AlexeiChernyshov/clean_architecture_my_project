import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';

import '../../resources/app_path.dart';
import '../../resources/app_routes.dart';
import '../../resources/app_strings.dart';

// Экраны без нижней панели навигации
final List<String> _screensWithoutNavigationBar = [
  AppRoutes.testScreenThreeRoute,
];
const Widget _navigationBar = BottomNavigation();

void _onItemTapped(int index, BuildContext context) {
  switch (index) {
    case 0:
      context.goNamed(AppRoutes.newsRoute);
      break;
    case 1:
      context.goNamed(AppRoutes.promoRoute);
      break;
  }
}

bool _showNavigationBar(BuildContext context) {
  final String location = GoRouterState
      .of(context)
      .location;
  for (String screen in _screensWithoutNavigationBar) {
    if (location.contains(screen)) {
      return false;
    }
  }
  return true;
}

int _getSelectedIndex(BuildContext context) {
  final String location = GoRouterState.of(context).location;
  if (location.startsWith(AppPath.newsScreenPath)) {
    return 0;
  }
  if (location.startsWith(AppPath.promoScreenPath)) {
    return 1;
  }
  return 0;
}

class MainNavigationBar extends StatelessWidget {
  const MainNavigationBar({
    Key? key,
    required this.child,
  }) : super(key: key);

  final Widget child;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: WillPopScope(
        onWillPop: () async {
          final String location = GoRouterState.of(context).location;
          if (!location.startsWith(AppPath.newsScreenPath)) {
            _onItemTapped(0, context);
          }
          return true;
        },
        child: child,
      ),
      bottomNavigationBar: _showNavigationBar(context) ?
      _navigationBar : null,
    );
  }
}

class BottomNavigation extends StatelessWidget {
  const BottomNavigation({Key? key}) : super(key: key);
  final List<BottomNavigationBarItem> _items = const [
    BottomNavigationBarItem(
      label: NavigationBarStrings.news,
      icon: Icon(
        Icons.mark_unread_chat_alt_outlined,
        size: 24,
      ),
    ),
    BottomNavigationBarItem(
      label: NavigationBarStrings.promo,
      icon: Icon(
        Icons.paid_rounded,
        size: 24,
      ),
    ),
  ];

  @override
  Widget build(BuildContext context) {
    return BottomNavigationBar(
      items: _items,
      currentIndex: _getSelectedIndex(context),
      onTap: (index) => _onItemTapped(index, context),
    );
  }
}
