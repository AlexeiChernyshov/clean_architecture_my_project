import 'package:flutter/cupertino.dart';
import 'package:go_router/go_router.dart';

import '../../main_sections/news_section/view/screens/news_screen.dart';
import '../../main_sections/news_section/view/screens/test_screen_one.dart';
import '../../main_sections/promo_section/view/screens/promo_screen.dart';
import '../../main_sections/promo_section/view/screens/test_screen_three.dart';
import '../../main_sections/promo_section/view/screens/test_screen_two.dart';
import '../presentation/pages/bottom_navigation_bar.dart';
import 'app_path.dart';
import 'app_routes.dart';


class AppRouter {
  static GoRouter router = GoRouter(
    initialLocation: AppPath.newsScreenPath,
    routes: [
      ShellRoute(
        builder: (context, state, child) => MainNavigationBar(child: child),
        routes: [
          GoRoute(
            name: AppRoutes.newsRoute,
            path: AppPath.newsScreenPath,
            pageBuilder: (context, state) => const NoTransitionPage(
              child: WrapperNewsScreenWidget(),
            ),
            routes: [
              GoRoute(
                name: AppRoutes.testScreenOneRoute,
                path: AppPath.testScreenOnePath,
                pageBuilder: (context, state) => const CupertinoPage(
                  child: TestScreenOneWidget(),
                ),
              ),
            ],
          ),
          GoRoute(
            name: AppRoutes.promoRoute,
            path: AppPath.promoScreenPath,
            pageBuilder: (context, state) => const NoTransitionPage(
              child: WrapperPromoScreenWidget(),
            ),
            routes: [
              GoRoute(
                name: AppRoutes.testScreenTwoRoute,
                path: AppPath.testScreenTwoPath,
                pageBuilder: (context, state) => const CupertinoPage(
                  child: TestScreenTwoWidget(),
                ),
                routes: [
                  GoRoute(
                    name: AppRoutes.testScreenThreeRoute,
                    path: AppPath.testScreenThreePath,
                    pageBuilder: (context, state) => const CupertinoPage(
                      child: TestScreenThreeWidget(),
                    ),
                  ),
                ]
              ),
            ],
          ),
        ],
      )
    ],
  );
}
