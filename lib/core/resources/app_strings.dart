class AppStrings {
}

class PromoSectionStrings {
  static const String testScreenTwo = 'TestScreenTwo';
  static const String goToTestScreenThree = 'Перейти на testScreenThree';
  static const String goToTestScreenTwo = 'Перейти на testScreenTwo';
  static const String testScreenThree = 'TestScreenThree';
  static const String update = 'Обновить';
  static const String loadingError = 'Произошла ошибка, попробуйте снова';
}

class NewsSectionStrings {
  static const String goToTestScreenOne = 'Перейти на testScreenOne';
  static const String testScreenOne = 'TestScreenOne';
  static const String downloadNews = 'Загрузить новости';
  static const String update = 'Обновить';
}

class NavigationBarStrings{
  static const String news = 'Новости';
  static const String promo = 'Акции';
  static const String empty = 'Пусто';
}