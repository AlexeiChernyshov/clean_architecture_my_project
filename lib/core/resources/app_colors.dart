import 'package:flutter/material.dart';

class AppColors {
  static const primary = Color(0xff50a7f6);
  static const navigationBarBackground = Color(0xffc3bcbc);
  static const secondary = Color(0xff272b30);
  static const primaryBackground = Color(0xfff4f0f0);
  static const secondaryBackground = Color(0xff272b30);
  static const primaryText = Color(0xffa9aaac);
  static const secondaryText = Colors.black;
  static const primaryBtnText = Colors.white;
  static const error = Colors.red;
  static const black = Colors.black;
  static const inactiveColor = Color(0x26ffffff);
}
